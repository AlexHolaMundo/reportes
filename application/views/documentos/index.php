<main id="main" class="main">
	<div class="pagetitle">
		<h1>Documento</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
				<li class="breadcrumb-item">Documento</li>
			</ol>
		</nav>
		<section class="section">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Registro de Informes</h5>
							<!-- Multi Columns Form -->
							<form class="row g-3" method="POST" action="<?php echo site_url('documentos/guardar') ?>" enctype="multipart/form-data" id="formDocumentos">
								<div class="col-md-3">
    <label for="" class="form-label white-text"><b>Autor</b></label>
    <?php foreach ($listadoAutores as $autor) : ?>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="autor_id[]" id="autor_<?php echo $autor->id_aut; ?>" value="<?php echo $autor->id_aut; ?>">
            <label class="form-check-label" for="autor_<?php echo $autor->id_aut; ?>">
                <?php echo $autor->nombre; ?> <?php echo $autor->apellido; ?>
            </label>
        </div>
    <?php endforeach; ?>
</div>

								<div class="col-md-3">
									<label for="" class="form-label white-text"><b>Articulo</b></label>
									<select name="articulo_id" id="articulo_id" class="form-control" required>
										<option value="">Selecciona un Artículo</option>
										<?php foreach ($listadoArticulos as $articulo) : ?>
											<option value="<?php echo $articulo->id_art; ?>"><?php echo $articulo->titulo; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="" class="form-label white-text"><b>Revista</b></label>
									<select name="revista_id" id="revista_id" class="form-control" required>
										<option value="">Selecciona una Revista</option>
										<?php foreach ($listadoRevistas as $revista) : ?>
											<option value="<?php echo $revista->id_rev; ?>"><?php echo $revista->titulo; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="" class="form-label white-text"><b>Arbitraje</b></label>
									<select name="arbitraje_id" id="arbitraje_id" class="form-control" required>
										<option value="">Selecciona un Arbitraje</option>
										<?php foreach ($listadoArbitrajes as $arbitraje) : ?>
											<option value="<?php echo $arbitraje->id_arb; ?>"><?php echo $arbitraje->estado; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-outline-success">
										Registrar <i class="bx bx-save"></i>
									</button>
									<button type="reset" class="btn btn-outline-danger">
										Cancelar <i class="bx bx-message-square-x"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Generar PDF</h5>
						<?php if ($listadoDocumentos) : ?>
							<table class="table w-100" id="tableDocumentos">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Autor</th>
										<th class="text-center">Artículo</th>
										<th class="text-center">Revista</th>
										<th class="text-center">ISSN</th>
										<th class="text-center">Volumen</th>
										<th class="text-center">Número</th>
										<th class="text-center">URL</th>
										<th class="text-center">Estado Arbitraje</th>
										<th class="text-center">Generar</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listadoDocumentos as $documento) : ?>
										<tr>
											<td class="text-center"><?php echo $documento->id_doc; ?></td>
											<td class="text-center"><?php echo $documento->nombre_autor . ' ' . $documento->apellido_autor; ?></td>
											<td class="text-center"><?php echo $documento->titulo_articulo; ?></td>
											<td class="text-center"><?php echo $documento->titulo_revista; ?></td>
											<td class="text-center"><?php echo $documento->issn_revista; ?></td>
											<td class="text-center"><?php echo $documento->volumen_revista; ?></td>
											<td class="text-center"><?php echo $documento->numero_revista; ?></td>
											<td class="text-center">
												<a class="url" href="<?php echo $documento->url_revista; ?>"><?php echo $documento->url_revista; ?></a>
											</td>
											<td class="text-center"><?php echo $documento->estado_arbitraje; ?></td>
											<td class="text-center">
												<a href="<?php echo site_url('documentos/descargarPDF/' . $documento->id_doc); ?>" class="btn btn-danger" title="Descargar PDF" target="_blank">
													<i class="bi bi-file-earmark-pdf"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No existen informes disponibles.
							</div>
						<?php endif; ?>
					</div>

				</div>
			</div>
		</section>
	</div>
</main>
<script>


    $(document).ready(function() {
        $('#formDocumentos').validate({
            rules: {
                autor_id: {
                    required: true,
                },
                 articulo_id: {
                    required: true,
                },
                revista_id: {
                  required: true,
                },
                arbitraje_id: {
                  required: true,
                }
            },
            messages: {
                autor_id: {
                    required: "Escoja por favor",
                },
                 articulo_id: {
                    required: "Escoja por favor",
                },
                revista_id: {
                  required: "Escoja por favor",
                },
				arbitraje_id: {
                  required: "Escoja por favor",
                }
            }
        });
    });
</script>
