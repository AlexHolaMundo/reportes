<main id="main" class="main">
  <div class="pagetitle">
    <h1>Revistas</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Revistas</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Editar Revistas</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('revistas/actualizar') ?>"enctype="multipart/form-data" id="formRevistas">
             <input type="hidden" name="id_rev" id="id_rev" value="<?php echo $revistaEditar->id_rev; ?>">  

              <div class="col-md-6">
                <label class="form-label" for="titulo">Título</label>
                <input type="text" class="form-control" id="titulo" value="<?php echo $revistaEditar->titulo; ?>" name="titulo" />
              </div>
              <div class="col-md-6">
                <label class="form-label" for="issn">ISSN</label>
                <input type="text" class="form-control" id="issn" value="<?php echo $revistaEditar->issn; ?>" name="issn" />
              </div>
              <div class="col-md-6">
                <label for="volumen" class="form-label">Volumen</label>
                <input type="number" class="form-control" id="volumen" value="<?php echo $revistaEditar->volumen; ?>" name="volumen" />
              </div>
              <div class="col-md-6">
                <label for="numero" class="form-label">Número</label>
                <input type="number" class="form-control" id="numero" value="<?php echo $revistaEditar->numero; ?>" name="numero" />
              </div>
              <div class="col-md-6">
                <label for="url" class="form-label">URL</label>
                <input type="text" class="form-control" id="url" value="<?php echo $revistaEditar->url; ?>" name="url" />
              </div>
              <div class="col-md-4">
                    <label for="" class="form-label white-text"><b>Comite Editorial:</b></label>
                    <select name="editorial_id" id="editorial_id" class="form-control" required>
                        <option value="">Selecciona un Comite Editorial</option>
                        <?php foreach ($Comites_Editoriales as $comite_editorial): ?>
                            <option value="<?php echo $comite_editorial->id_comite; ?>" <?php echo ($comite_editorial->id_comite == $revistaEditar->editorial_id) ? 'selected' : ''; ?>><?php echo $comite_editorial->nombre; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>


              <div class="text-center"><br>
                <button type="submit" name="button" class="btn btn-outline-warning">
                Actualizar <i class="bx bx-refresh" ></i></button> &nbsp &nbsp
                <a type="reset" href="<?php echo site_url('revistas/index'); ?>" class="btn btn-outline-danger">
                  Cancelar <i class="bx bx-message-square-x"></i>
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    
    <!-- End Recent Ventas -->
    
  </section>
</main>
<!-- End #main -->
<script>
    $(document).ready(function() {
        $('#formRevistas').validate({
            rules: {
                titulo: {
                    required: true,
                    letras: true
                },
                issn: {
                    required: true,
                },
                volumen: {
                    required: true,
                },
                numero: {
                    required: true,
                },
                url: {
                    required: true,
                    validUrl: true
                },
                editorial_id: {
                    required: true,
                }
            },
            messages: {
                titulo: {
                    required: "Ingrese el titulo por favor",
                    letras: "Ingrese solo letras por favor"
                },
                issn: {
                    required: "Ingrese el ISSN por favor",
                },
                volumen: {
                    required: "Ingrese el número del volumen por favor",
                },
                numero: {
                    required: "Ingrese el número por favor",
                },
                url: {
                    required: "Ingrese la url por favor",
                },
                editorial_id: {
                    required: "Seleccione un Comite-Editorial por favor",
                }
            }
        });
        $.validator.addMethod("letras", function(value, element) {
            return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/.test(value);
        }, "Solo se permiten letras");
        $.validator.addMethod("validUrl", function(value, element) {
            // Utilizamos una expresión regular para validar la URL
            return this.optional(element) || /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i.test(value);
        }, "Ingrese una URL válida (debe comenzar con http:// o https://)");
    });
</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>