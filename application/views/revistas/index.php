<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<main id="main" class="main">
	<div class="pagetitle">
		<h1>Revistas</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
				<li class="breadcrumb-item">Revistas</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Registro de Revistas</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('revistas/guardar') ?>" enctype="multipart/form-data" id="formRevistas">

							<div class="col-md-6">
								<label class="form-label" for="titulo">Título</label>
								<input type="text" class="form-control" id="titulo" name="titulo" />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="issn">ISSN</label>
								<input type="text" class="form-control" id="issn" name="issn" />
							</div>
							<div class="col-md-6">
								<label for="volumen" class="form-label">Volumen</label>
								<input type="number" class="form-control" id="volumen" name="volumen" />
							</div>
							<div class="col-md-6">
								<label for="numero" class="form-label">Número</label>
								<input type="number" class="form-control" id="numero" name="numero" />
							</div>
							<div class="col-md-6">
								<label for="url" class="form-label">URL</label>
								<input type="text" class="form-control" id="url" name="url" />
							</div>
							<div class="col-md-6">
								<label for="" class="form-label white-text"><b>Comite Editorial:</b></label>
								<select name="editorial_id" id="editorial_id" class="form-control" required>
									<option value="">Selecciona un Comite Editorial</option>
									<?php foreach ($Comites_Editoriales as $comite_editorial) : ?>
										<option value="<?php echo $comite_editorial->id_comite; ?>"><?php echo $comite_editorial->nombre; ?></option>
									<?php endforeach; ?>
								</select>
							</div>

							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
									Registrar <i class="bx bx-save"></i>
								</button>
								<button type="reset" class="btn btn-outline-danger">
									Cancelar <i class="bx bx-message-square-x"></i>
								</button>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Recent Ventas -->
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Lista de Revistas</h5>
						<?php if ($listadoRevistas) : ?>
							<table class="table w-100" id="tableRevistas">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">TITULO</th>
										<th class="text-center">ISSN</th>
										<th class="text-center">VOLUMEN</th>
										<th class="text-center">NÚMERO</th>
										<th class="text-center">URL</th>
										<th class="text-center">COMITE-EDITORIAL</th>
										<th class="text-center">ACCIONES</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listadoRevistas as $revista) : ?>
										<tr>
											<td class="text-center"><?php echo $revista->id_rev; ?></td>
											<td class="text-center"><?php echo $revista->titulo; ?></td>
											<td class="text-center"><?php echo $revista->issn; ?></td>
											<td class="text-center"><?php echo $revista->volumen; ?></td>
											<td class="text-center"><?php echo $revista->numero; ?></td>
											<td class="text-center w-25"><?php echo $revista->url; ?></td>
											<td class="text-center"><?php echo $revista->nombre; ?></td>
											<td class="text-center">
												<a href="<?php echo site_url('revistas/borrar/') . $revista->id_rev; ?>"
													class="btn btn-outline-warning btn-eliminar" title="Borrar">
													<i class="bi bi-trash3-fill"></i> 
												</a>
												<a href="<?php echo site_url('revistas/editar/') . $revista->id_rev; ?>" class=" btn btn-outline-primary" title="Editar">
													<i class="bi bi-pen"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No se encontro revistas registrados
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- End Recent Ventas -->
		</div>
	</section>
</main>
<!-- End #main -->
<script>
	$(document).ready(function() {
		$('#formRevistas').validate({
			rules: {
				titulo: {
					required: true,
					letras: true
				},
				issn: {
					required: true,
				},
				volumen: {
					required: true,
				},
				numero: {
					required: true,
				},
				url: {
					required: true,
					validUrl: true
				},
				editorial_id: {
					required: true,
				}
			},
			messages: {
				titulo: {
					required: "Ingrese el titulo por favor",
					letras: "Ingrese solo letras por favor"
				},
				issn: {
					required: "Ingrese el ISSN por favor",
				},
				volumen: {
					required: "Ingrese el número del volumen por favor",
				},
				numero: {
					required: "Ingrese el número por favor",
				},
				url: {
					required: "Ingrese la url por favor",
				},
				editorial_id: {
					required: "Seleccione un Comite-Editorial por favor",
				}
			}
		});
		$.validator.addMethod("letras", function(value, element) {
			return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/.test(value);
		}, "Solo se permiten letras");
		$.validator.addMethod("validUrl", function(value, element) {
			// Utilizamos una expresión regular para validar la URL
			return this.optional(element) || /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i.test(value);
		}, "Ingrese una URL válida (debe comenzar con http:// o https://)");
	});
</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>
<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: '¿Estás seguro?',
        text: "Esta acción no se puede deshacer",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>