<cript src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- Bootstrap Fileinput CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

<main id="main" class="main">
  <div class="pagetitle">
    <h1>Comite-Editorial</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Comite-Editorial.</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Editar Comite Editorial</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('Comites_Editoriales/actualizar') ?>"enctype="multipart/form-data" id="formComitesEditoria">
            <input type="hidden" name="id_comite" id="id_comite" value="<?php echo $comiteEditar->id_comite; ?>">
              <div class="col-md-6">
                <label class="form-label" for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" value="<?php echo $comiteEditar->nombre; ?>" name="nombre" />
              </div>
              <div class="col-md-6">
                <label class="form-label" for="cargo_comite">Cargo Comite</label>
                <input type="text" class="form-control" id="cargo_comite" value="<?php echo $comiteEditar->cargo_comite; ?>" name="cargo_comite" />
              </div>
              <div class="col-md-12">
                    <label for="firma" class="form-label">Firma</label>
                    <input type="file" class="form-control" id="firma" name="firma" placeholder="Suba su firma" accept=".pdf"/><br>
                    <?php if ($comiteEditar->firma != ""): ?>
                        <p>
                            <a href="<?php echo base_url('uploads/comites/') . $comiteEditar->firma; ?>" target="_blank">
                                <i class="fas fa-file-pdf fa-3x"></i>
                            </a>
                            <?php echo $comiteEditar->firma; ?>
                        </p>
                    <?php else: ?>
                        <p>No hay PDF existente</p>
                    <?php endif; ?>
                </div>

              <div class="text-center"><br>
                <button type="submit" name="button" class="btn btn-outline-warning">
                Actualizar <i class="bx bx-refresh" ></i></button> &nbsp &nbsp
                <a type="reset" href="<?php echo site_url('Comites_Editoriales/index'); ?>" class="btn btn-outline-danger">
                  Cancelar <i class="bx bx-message-square-x"></i>
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<!-- End #main -->
<script>
    $(document).ready(function () {
        $("#firma").fileinput({
            language: 'es',
        });
    });

    $(document).ready(function() {
        $('#formComitesEditorial').validate({
            rules: {
                nombre: {
                    required: true,
                    letras: true
                },
                cargo_comite: {
                    required: true,
                },
                firma: {
                    required: true,
                    accept: "application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                }
            },
            messages: {
                nombre: {
                    required: "Ingrese el nombre por favor",
                    letras: "Ingrese solo letras por favor"
                },
                cargo_comite: {
                    required: "Ingrese el cargo por favor",
                },
                firma: {
                    required: "Suba la firma por favor",
                    accept: "Por favor, seleccione un archivo PDF o de Word válido"
                }
            }
        });
        $.validator.addMethod("letras", function(value, element) {
            return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/.test(value);
        }, "Solo se permiten letras");
    });
</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>