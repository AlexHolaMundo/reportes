<cript src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- Bootstrap Fileinput CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<main id="main" class="main">
  <div class="pagetitle">
    <h1>Comite-Editorial</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Comite Editorial.</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Registro de Comite-Editorial</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('Comites_Editoriales/guardar') ?>"
              enctype="multipart/form-data" id="formComitesEditorial">

              <div class="col-md-6">
                <label class="form-label" for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" />
              </div>
              <div class="col-md-6">
                <label class="form-label" for="cargo_comite">Cargo Comite</label>
                <input type="text" class="form-control" id="cargo_comite" name="cargo_comite" />
              </div>
              <div class="col-md-12">
                <label for="firma" class="form-label">Firma</label>
                <input type="file" class="form-control" id="firma" name="firma" placeholder="Suba su firma" accept=".pdf"/>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-outline-success">
                  Registrar <i class="bx bx-save"></i>
                </button>
                <button type="reset" class="btn btn-outline-danger">
                  Cancelar <i class="bx bx-message-square-x"></i>
                </button>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Recent Ventas -->
      <div class="col-12">
        <div class="card recent-sales overflow-auto">
          <div class="card-body">
            <h5 class="card-title">Lista de Comite-Editorial</h5>
            <?php if ($listadoComites) : ?>
            <table class="table w-100" id="tableCargos">
              <thead>
                <tr>
                  <th class="text-center">ID</th>
                  <th class="text-center">NOMBRE</th>
                  <th class="text-center">CARGO</th>
                  <th class="text-center">FIRMA</th>
                  <th class="text-center">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($listadoComites as $comite_editorial) : ?>
                <tr>
                  <td class="text-center"><?php echo $comite_editorial->id_comite; ?></td>
                  <td class="text-center"><?php echo $comite_editorial->nombre; ?></td>
                  <td class="text-center"><?php echo $comite_editorial->cargo_comite; ?></td>
                  <td class="text-center">
                    <?php if ($comite_editorial->firma != ""): ?>
                      <a href="<?php echo base_url('uploads/comites/') . $comite_editorial->firma; ?>" target="_blank">
                          <i class="fas fa-file-pdf fa-3x"></i> <!-- Icono de PDF -->
                      </a>
                    <?php else: ?>
                        N/A
                    <?php endif; ?>
                  </td>
                  <td class="text-center">
                    <a href="<?php echo site_url('Comites_Editoriales/borrar/') . $comite_editorial->id_comite; ?>"
                      class="btn btn-outline-warning btn-eliminar" title="Borrar">
                      <i class="bi bi-trash3-fill"></i> 
                    </a>


                    <a href="<?php echo site_url('Comites_Editoriales/editar/') . $comite_editorial->id_comite; ?>"
                      class=" btn btn-outline-primary" title="Editar">
                      <i class="bi bi-pen"></i>
                    </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php else : ?>
            <div class="alert alert-danger">
              No se encontro cargos registrados
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Recent Ventas -->
    </div>
  </section>
</main>
<!-- End #main -->

<script>
    $(document).ready(function () {
        $("#firma").fileinput({
            language: 'es',
        });
    });

    $(document).ready(function() {
        $('#formComitesEditorial').validate({
            rules: {
                nombre: {
                    required: true,
                    letras: true
                },
                cargo_comite: {
                    required: true,
                },
                firma: {
                    required: true,
                    accept: "application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                }
            },
            messages: {
                nombre: {
                    required: "Ingrese el nombre por favor",
                    letras: "Ingrese solo letras por favor"
                },
                cargo_comite: {
                    required: "Ingrese el cargo por favor",
                },
                firma: {
                    required: "Suba la firma por favor",
                    accept: "Por favor, seleccione un archivo PDF o de Word válido"
                }
            }
        });
        $.validator.addMethod("letras", function(value, element) {
            return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/.test(value);
        }, "Solo se permiten letras");
    });
</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>

<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: '¿Estás seguro?',
        text: "Esta acción no se puede deshacer",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>

