<main id="main" class="main">
	<div class="pagetitle">
		<h1>Autores</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
				<li class="breadcrumb-item">Autores</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Actualizar Autores</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('autores/actualizarAutor') ?>" enctype="multipart/form-data" id="formAutores">
							<input type="hidden" name="id_aut" id="id_aut" value="<?php echo $autorEditar->id_aut; ?>">
							<div class="col-md-6">
								<label class="form-label" for="nombre">Nombre</label>
								<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $autorEditar->nombre; ?>" required />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="apellido">Apellido</label>
								<input type="text" class="form-control" id="apellido" name="apellido" value="<?php echo $autorEditar->apellido; ?> " required />
							</div>
							<div class="col-md-6">
								<label for="" class="form-label">Email</label>
								<input type="eemail" class="form-control" id="email" name="email" value="<?php echo $autorEditar->email; ?> " required />
							</div>
							<div class="col-md-6">
								<label for="" class="form-label">Telefono</label>
								<input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $autorEditar->telefono; ?>" required />
							</div>
							<div class="col-md-6">
								<label for="" class="form-label">Pais</label>
								<input type="text" class="form-control" id="pais" name="pais" value="<?php echo $autorEditar->pais; ?>" required />
							</div>
							<div class="col-md-6">
								<label for="" class="form-label">Ciudad</label>
								<input type="text" class="form-control" id="ciudad" name="ciudad" value="<?php echo $autorEditar->ciudad; ?>" required />
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
									Actualizar <i class="bx bx-save"></i>
								</button>

								<a href="<?php echo site_url('autores/index'); ?>" class="btn btn-outline-danger">
									<i class="bx bx-message-square-x"></i>
									Cancelar
								</a>

							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>

	</section>
</main>
<!-- End #main -->
