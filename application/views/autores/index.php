<main id="main" class="main">
	<div class="pagetitle">
		<h1>Autores</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
				<li class="breadcrumb-item">Autores</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Registro de Autores</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('autores/guardar') ?>" enctype="multipart/form-data" id="formAutores">

							<div class="col-md-6">
								<label class="form-label" for="nombre">Nombre</label>
								<input type="text" class="form-control" id="nombre" name="nombre" />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="apellido">Apellido</label>
								<input type="text" class="form-control" id="apellido" name="apellido" />
							</div>
							<div class="col-md-6">
								<label for="email" class="form-label">Email</label>
								<input type="email" class="form-control" id="email" name="email" />
							</div>
							<div class="col-md-6">
								<label for="telefono" class="form-label">Telefono</label>
								<input type="text" class="form-control" id="telefono" name="telefono" />
							</div>
							<div class="col-md-6">
								<label for="pais" class="form-label">Pais</label>
								<input type="text" class="form-control" id="pais" name="pais" />
							</div>
							<div class="col-md-6">
								<label for="ciudad" class="form-label">Ciudad</label>
								<input type="text" class="form-control" id="ciudad" name="ciudad" />
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
									Registrar <i class="bx bx-save"></i>
								</button>
								<button type="reset" class="btn btn-outline-danger">
									Cancelar <i class="bx bx-message-square-x"></i>
								</button>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Recent Ventas -->
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Lista de Autores</h5>
						<?php if ($listadoAutores) : ?>
							<table class="table w-100" id="tableAutores">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">NOMBRE</th>
										<th class="text-center">APELLIDO</th>
										<th class="text-center">EMAIL</th>
										<th class="text-center">TELEFONO</th>
										<th class="text-center">PAIS</th>
										<th class="text-center">CIUDAD</th>
										<th class="text-center">ACCIONES</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listadoAutores as $autor) : ?>
										<tr>
											<td class="text-center"><?php echo $autor->id_aut; ?></td>
											<td class="text-center"><?php echo $autor->nombre; ?></td>
											<td class="text-center"><?php echo $autor->apellido; ?></td>
											<td class="text-center"><?php echo $autor->email; ?></td>
											<td class="text-center"><?php echo $autor->telefono; ?></td>
											<td class="text-center"><?php echo $autor->pais; ?></td>
											<td class="text-center"><?php echo $autor->ciudad; ?></td>
											<td class="text-center">
												<a href="<?php echo site_url('autores/borrar/') . $autor->id_aut; ?>" class=" btn btn-outline-warning" title="Borrar">
													<i class="bi bi-trash3-fill"></i>
												</a>
												<a href="<?php echo site_url('autores/editar/') . $autor->id_aut; ?>" class=" btn btn-outline-primary" title="Editar">
													<i class="bi bi-pen"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No se encontro autores registrados
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- End Recent Ventas -->
		</div>
	</section>
</main>
<script>
	$(document).ready(function() {
	$.validator.addMethod(
		'lettersonly',
		function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
		},
		'Por favor, ingrese solo letras'
	)

	$('#formAutores').validate({
		rules: {
			nombre: {
				required: true,
				lettersonly: true,
				maxlength: 30,
				minlength: 3,
			},
			apellido: {
				required: true,
				lettersonly: true,
				maxlength: 30,
				minlength: 3,
			},
			email: {
				required: true,
				email: true,
			},
			telefono: {
				required: true,
				number: true,
				minlength: 7,
				maxlength: 10,
			},
			pais: {
				required: true,
				lettersonly: true,
				maxlength: 50,
				minlength: 3,
			},
			ciudad: {
				required: true,
				lettersonly: true,
				maxlength: 50,
				minlength: 3,
			},
		},
		messages: {
			nombre: {
				required: 'El nombre es obligatorio',
				lettersonly: 'Solo se permiten letras en este campo',
				maxlength: 'El nombre debe tener menos de 30 caracteres',
				minlength: 'El nombre debe tener al menos 3 caracteres',
			},
			apellido: {
				required: 'El apellido es obligatorio',
				lettersonly: 'Solo se permiten letras en este campo',
				maxlength: 'El apellido debe tener menos de 30 caracteres',
				minlength: 'El apellido debe tener al menos 3 caracteres',
			},
			email: {
				required: 'El email es obligatorio',
         		email: 'El email debe ser válido',
			},
			telefono: {
				required: 'El telefono es obligatorio',
				number: 'El telefono debe ser un número',
				minlength: 'El telefono debe tener al menos 7 dígitos',
				maxlength: 'El telefono debe tener menos de 10 dígitos',
			},
			pais: {
				required: 'El pais es obligatoria',
          lettersonly: 'Solo se permiten letras en este campo',
				maxlength: 'el pais debe tener menos de 50 caracteres',
				minlength: 'El pais debe tener al menos 3 caracteres',
			},
			ciudad: {
				required: 'La ciudad es obligatoria',
          		lettersonly: 'Solo se permiten letras en este campo',
				maxlength: 'La ciudad debe tener menos de 50 caracteres',
				minlength: 'La ciudad debe tener al menos 3 caracteres',
			},
		},
	})
})
</script>
<!-- End #main -->
