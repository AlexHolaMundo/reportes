<main id="main" class="main">
  <div class="pagetitle">
    <h1>Arbitrajes</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Arbitrajes</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Editar Arbitrajes</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="post" action="<?php echo site_url('arbitrajes/actualizarArbitraje'); ?>" enctype="multipart/form-data" id="formArbitrajes">
              <input type="hidden" name="id_arb" id="id_arb" value="<?php echo $arbitrajeEditar->id_arb;?>">  
              <div class="row">
                <div class="col-md-6">
                  <div class="col-md-12">
                    <label class="form-label" for="estado"><b>Estado</b></label>
                    <input value="<?php echo $arbitrajeEditar->estado; ?>" type="text" class="form-control" id="estado" name="estado" />
                  </div>
              
                  <div class="col-md-12">
                    <label class="form-label" for="fecha_desicion"><b>Fecha de Decisión</b></label>
                    <input value="<?php echo $arbitrajeEditar->fecha_desicion; ?>" type="date" class="form-control" id="fecha_desicion" name="fecha_desicion" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="col-md-12">
                      <label class="form-label" for="comentario"><b>Comentario</b></label>
                      <textarea class="form-control" id="comentario" name="comentario" style="height: 110px;"><?php echo $arbitrajeEditar->comentario; ?></textarea>
                  </div>

                </div>
              </div>  
              <div class="text-center"><br>
                <button type="submit" name="button" class="btn btn-outline-warning">
                Actualizar <i class="bx bx-refresh" ></i></button> &nbsp &nbsp
                <a type="reset" href="<?php echo site_url('arbitrajes/index'); ?>" class="btn btn-outline-danger">
                  Cancelar <i class="bx bx-message-square-x"></i>
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>

<script>
$(document).ready(function () {
    $("#logo").fileinput({
        language: 'es',
    });
});

$(document).ready(function() {
    $('#formArbitrajes').validate({
        rules: {
            estado: {
                required: true,
            },
              comentario: {
                required: true,
            },
            fecha_desicion: {
              required: true,
              dateRange: true 
            },
            documento_id: {
                required: true,
            }
        },
        messages: {
            estado: {
                required: "Ingrese el estado",
            },
              comentario: {
                required: "Especifique un comentario porfavor",
            },
            fecha_desicion: {
              required: "Ingrese la fecha de decisión",
              dateRange: "La fecha debe estar dentro del último mes"
            },
            documento_id: {
                required: "Seleccione la revista",
            }
        }
    });
});
$.validator.addMethod("dateRange", function(value, element) {
    var currentDate = new Date();
    var oneMonthAgo = new Date(currentDate);
    oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
    
    var selectedDate = new Date(value);
    
    return selectedDate >= oneMonthAgo && selectedDate <= currentDate;
}, "La fecha debe estar dentro del último mes");

</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>
