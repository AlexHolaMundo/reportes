<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<main id="main" class="main">
  <div class="pagetitle">
    <h1>Arbitrajes</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Arbitrajes</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Registro de Arbitrajes</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('arbitrajes/guardar') ?>"
              enctype="multipart/form-data" id="formArbitrajes">
              <div class="row">
                <div class="col-md-6">
                  <div class="col-md-12">
                    <label class="form-label" for="estado"><b>Estado</b></label>
                    <input type="text" class="form-control" id="estado" name="estado" />
                  </div>
              
                  <div class="col-md-12">
                    <label class="form-label" for="fecha_desicion"><b>Fecha de Decisión</b></label>
                    <input type="date" class="form-control" id="fecha_desicion" name="fecha_desicion" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="col-md-12">
                      <label class="form-label" for="comentario"><b>Comentario</b></label>
                      <textarea class="form-control" id="comentario" name="comentario" style="height: 110px;"></textarea>
                  </div>
                </div>
              </div>
              <div class="text-center"><br>
                <button type="submit" class="btn btn-outline-success">
                  Registrar <i class="bx bx-save"></i>
                </button>
                <button type="reset" class="btn btn-outline-danger">
                  Cancelar <i class="bx bx-message-square-x"></i>
                </button>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Recent Ventas -->
      <div class="col-12">
        <div class="card recent-sales overflow-auto">
          <div class="card-body">
            <h5 class="card-title">Lista de Documentos</h5>
            <?php if ($listadoArbitraje) : ?>
            <table class="table w-100" id="tableArbitrajes">
              <thead>
                <tr>
                  <th class="text-center">ID</th>
                  <th class="text-center">ESTADO</th>
                  <th class="text-center">COMENTARIO</th>
                  <th class="text-center">FECHA DECISION</th>
                  <th class="text-center">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($listadoArbitraje as $arbitraje) : ?>
                <tr>
                  <td class="text-center"><?php echo $arbitraje->id_arb; ?></td>
                  <td class="text-center"><?php echo $arbitraje->estado; ?></td>
                  <td class="text-center"><?php echo $arbitraje->comentario; ?></td>
                  <td class="text-center"><?php echo $arbitraje->fecha_desicion; ?></td>
  
                  <td class="text-center">
                    <a href="<?php echo site_url('arbitrajes/borrar/') . $arbitraje->id_arb; ?>"
                      class=" btn btn-outline-warning btn-eliminar" title="Borrar">
                      <i class="bi bi-trash3-fill"></i>
                    </a>
                    <a href="<?php echo site_url('arbitrajes/editar/') . $arbitraje->id_arb; ?>"
                      class=" btn btn-outline-primary" title="Editar">
                      <i class="bi bi-pen"></i>
                    </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php else : ?>
            <div class="alert alert-danger">
              No se encontro arbitraje registrados
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Recent Ventas -->
    </div>
  </section>
</main>
<!-- End #main -->
<script>
    $(document).ready(function () {
        $("#logo").fileinput({
            language: 'es',
        });
    });

    $(document).ready(function() {
        $('#formArbitrajes').validate({
            rules: {
                estado: {
                    required: true,
                },
                 comentario: {
                    required: true,
                },
                fecha_desicion: {
                  required: true,
                  dateRange: true 
                }
            },
            messages: {
                estado: {
                    required: "Ingrese el estado",
                },
                 comentario: {
                    required: "Especifique un comentario porfavor",
                },
                fecha_desicion: {
                  required: "Ingrese la fecha de decisión",
                  dateRange: "La fecha debe estar dentro del último mes"
                }
            }
        });
    });
    $.validator.addMethod("dateRange", function(value, element) {
        var currentDate = new Date();
        var oneMonthAgo = new Date(currentDate);
        oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
        
        var selectedDate = new Date(value);
        
        return selectedDate >= oneMonthAgo && selectedDate <= currentDate;
    }, "La fecha debe estar dentro del último mes");

</script>


<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>
<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: '¿Estás seguro?',
        text: "Esta acción no se puede deshacer",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>