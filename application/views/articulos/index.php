<main id="main" class="main">
	<div class="pagetitle">
		<h1>Articulos</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
				<li class="breadcrumb-item">Articulos</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Registro de Articulos</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('articulos/guardar') ?>" enctype="multipart/form-data" id="formArticulos">

							<div class="col-md-6">
								<label class="form-label" for="titulo">Titulo:</label>
								<input type="text" class="form-control" id="titulo" name="titulo" required />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="fecha">Fecha:</label>
								<input type="date" class="form-control" id="fecha" name="fecha" required />
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
									Registrar <i class="bx bx-save"></i>
								</button>
								<button type="reset" class="btn btn-outline-danger">
									Cancelar <i class="bx bx-message-square-x"></i>
								</button>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Recent Ventas -->
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Lista de Articulos</h5>
						<?php if ($listadoArticulos) : ?>
							<table class="table w-100" id="tableArticulos">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">titulo</th>
										<th class="text-center">fecha</th>
										<th class="text-center">ACCIONES</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listadoArticulos as $articulo) : ?>
										<tr>
											<td class="text-center"><?php echo $articulo->id_art; ?></td>
											<td class="text-center"><?php echo $articulo->titulo; ?></td>
											<td class="text-center"><?php echo $articulo->fecha; ?></td>
											<td class="text-center">
												<a href="<?php echo site_url('articulos/borrar/') . $articulo->id_art; ?>" class=" btn btn-outline-warning" title="Borrar">
													<i class="bi bi-trash3-fill"></i>
												</a>
												<a href="<?php echo site_url('articulos/editar/') . $articulo->id_art; ?>" class=" btn btn-outline-primary" title="Editar">
													<i class="bi bi-pen"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No se encontro articulos registrados
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
</main>
<script>
	$(document).ready(function() {
	$.validator.addMethod(
		'lettersonly',
		function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
		},
		'Por favor, ingrese solo letras'
	)

	$('#formArticulos').validate({
		rules: {
			titulo: {
				required: true,
				lettersonly: true,
				maxlength: 100,
				minlength: 3,
			},
			fecha: {
				required: true,
          		date: true,
			},
			
		},
		messages: {
			titulo: {
				required: 'El titulo es obligatorio',
				lettersonly: 'Solo se permiten letras en este campo',
				maxlength: 'El titulo debe tener menos de 30 caracteres',
				minlength: 'El titulo debe tener al menos 3 caracteres',
			},
			fecha: {
				required: 'La fecha es obligatoria',
          		date: 'La fecha de entrega debe ser válida',
			},
			
		},
	})
})
</script>
<!-- End #main -->
