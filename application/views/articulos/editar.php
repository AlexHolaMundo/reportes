<main id="main" class="main">
  <div class="pagetitle">
    <h1>Articulos</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Articulos</li>
      </ol>
    </nav>
  </div>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12"> 
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Actualizar Articulos</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('articulos/actualizarArticulo') ?>"
              enctype="multipart/form-data" id="formArticulos">
              <input type="hidden" name="id_art" id="id_art" value="<?php echo $articuloEditar->id_art; ?>">

              <div class="col-md-6">
                <label class="form-label" for="titulo">Titulo:</label>
                <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $articuloEditar->titulo; ?>" required/>
              </div>
              <div class="col-md-6">
                <label class="form-label" for="fecha">Fecha:</label>
                <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $articuloEditar->fecha; ?>" required/>
              </div>
              
              <div class="text-center">
                <button type="submit" class="btn btn-outline-success">
                  Actualizar <i class="bx bx-save"></i>
                </button>
                <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-outline-danger"> 
                <i class="bx bx-message-square-x"></i> 
                Cancelar
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    
  </section>
</main>
<!-- End #main -->
