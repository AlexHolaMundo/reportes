<main id="main" class="main">
	<div class="pagetitle">
		<h1>Indexaciones</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
				<li class="breadcrumb-item">Indexaciones</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Editar Indexaciones</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="post" action="<?php echo site_url('indexaciones/actualizarIndexacion'); ?>" enctype="multipart/form-data" id="formIndexaciones">
							<input type="hidden" name="id_index" id="id_index" value="<?php echo $indexacionEditar->id_index; ?>">
							<div class="row">
								<div class="col-md-6">
									<label class="form-label" for="nombre"><b>Nombre</b></label>
									<input value="<?php echo $indexacionEditar->nombre; ?>" type="text" class="form-control" id="nombre" name="nombre" />
								</div>
								<div class="col-md-6">
									<label for="" class="form-label white-text"><b>Revista:</b></label>
									<select name="revista_id" id="revista_id" class="form-control" required>
										<option value="">Seleccione la revista</option>
										<?php foreach ($Revistas as $revista) : ?>
											<option value="<?php echo $revista->id_rev; ?>" <?php echo ($revista->id_rev == $indexacionEditar->revista_id) ? 'selected' : ''; ?>><?php echo $revista->titulo; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-12">
									<label class="form-label" for="issn"><b>Logo</b></label>
									<?php if (!empty($indexacionEditar->logo)) : ?>
										<img src="<?php echo base_url('uploads/indexaciones/' . $indexacionEditar->logo); ?>" alt="Foto del hospital" style="width: 250px; height: 250px;" class="rounded-circle">
									<?php endif; ?>
									<label class="form-label" for="issn"><b></b></label>
									<input type="file" name="logo" id="logo" value="" class="form-control" placeholder="Suba su logo" accept="image/*">
								</div>
							</div>
							<div class="text-center">
								<button type="submit" name="button" class="btn btn-outline-warning">
									Actualizar <i class="bx bx-refresh"></i></button> &nbsp &nbsp
								<a type="reset" href="<?php echo site_url('indexaciones/index'); ?>" class="btn btn-outline-danger">
									Cancelar <i class="bx bx-message-square-x"></i>
								</a>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<script>
		$(document).ready(function() {
			$("#logo").fileinput({
				language: 'es',
			});
		});

		$(document).ready(function() {
			$('#formIndexaciones').validate({
				rules: {
					nombre: {
						required: true,
					},

					revista_id: {
						required: true,
					}
				},
				messages: {
					nombre: {
						required: "Ingrese el nombre del logo",
					},

					revista_id: {
						required: "Seleccione la revista",
					}
				}
			});
		});
	</script>
<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>