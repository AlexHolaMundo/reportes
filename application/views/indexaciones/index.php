<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<main id="main" class="main">
	<div class="pagetitle">
		<h1>Indexaciones</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
				<li class="breadcrumb-item">Indexaciones</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Registro de Indexaciones</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('indexaciones/guardar') ?>" enctype="multipart/form-data" id="formIndexaciones">
							<div class="row">
								<div class="col-md-6">
									<label class="form-label" for="nombre"><b>Nombre</b></label>
									<input type="text" class="form-control" id="nombre" name="nombre" />
								</div>
								<div class="col-md-6">
									<label for="" class="form-label white-text"><b>Revista:</b></label>
									<select name="revista_id" id="revista_id" class="form-control" required>
										<option value="">Seleccione la revista</option>
										<?php foreach ($Revistas as $revistas) : ?>
											<option value="<?php echo $revistas->id_rev; ?>"><?php echo $revistas->titulo; ?></option>
										<?php endforeach; ?>
									</select>
								</div>

							</div>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-12">
									<label class="form-label" for="issn"><b>Logo</b></label>
									<input type="file" name="logo" id="logo" value="" class="form-control" placeholder="Suba su logo" accept="image/*" required>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
									Registrar <i class="bx bx-save"></i>
								</button>
								<button type="reset" class="btn btn-outline-danger">
									Cancelar <i class="bx bx-message-square-x"></i>
								</button>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Recent Ventas -->
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Lista de Indexaciones</h5>
						<?php if ($listadoIndexaciones) : ?>
							<table class="table w-100" id="tableIndexaciones">
								<thead>
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">TITULO</th>
										<th class="text-center">LOGO</th>
										<th class="text-center">REVISTA</th>
										<th class="text-center">ACCIONES</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listadoIndexaciones as $indexacion) : ?>
										<tr>
											<td class="text-center"><br><?php echo $indexacion->id_index; ?></td>
											<td class="text-center"><br><?php echo $indexacion->nombre; ?></td>
											<td class="text-center">
												<?php if ($indexacion->logo != "") : ?>
													<img src="<?php echo base_url('uploads/indexaciones/') . $indexacion->logo; ?>" height="100px" width="100px" alt="" class="rounded-circle">
												<?php else : ?>
													N/A
												<?php endif; ?>
											</td>
											<td class="text-center"><br><?php echo $indexacion->titulo; ?></td>

											<td class="text-center"><br>
												<a href="<?php echo site_url('indexaciones/borrar/') . $indexacion->id_index; ?>" class=" btn btn-outline-warning btn-eliminar" title="Borrar">
													<i class="bi bi-trash3-fill"></i>
												</a>
												<a href="<?php echo site_url('indexaciones/editar/') . $indexacion->id_index; ?>" class=" btn btn-outline-primary" title="Editar">
													<i class="bi bi-pen"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No se encontro indexaciones registrads
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- End Recent Ventas -->
		</div>
	</section>
</main>

<script>
	$(document).ready(function() {
		$("#logo").fileinput({
			language: 'es',
		});
	});

	$(document).ready(function() {
		$('#formIndexaciones').validate({
			rules: {
				nombre: {
					required: true,
				},
				logo: {
					required: true,
				},
				revista_id: {
					required: true,
				}
			},
			messages: {
				nombre: {
					required: "Ingrese el nombre del logo",
				},
				logo: {
					required: "Suba el logo por favor",
				},
				revista_id: {
					required: "Seleccione la revista",
				}
			}
		});
	});
</script>

<script>
  $(document).ready(function() {
    // Verificar si existe el mensaje en la sesión flash
    var mensaje = "<?php echo $this->session->flashdata('mensaje'); ?>";

    // Si hay un mensaje, mostrar la alerta
    if (mensaje) {
      // Crear la alerta con Bootstrap
      var alerta = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
      alerta += mensaje;
      alerta += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      alerta += '</div>';

      // Agregar la alerta al contenedor de mensajes
      $('.pagetitle').after(alerta);

      // Ocultar la alerta después de 5 segundos
      setTimeout(function() {
        $('.alert').alert('close');
      }, 5000); // 5000 milisegundos = 5 segundos
    }
  });
</script>
<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: '¿Estás seguro?',
        text: "Esta acción no se puede deshacer",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>
