<?php
class Arbitraje extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('arbitraje', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$arbitrajes = $this->db->get('arbitraje');
		if ($arbitrajes->num_rows() > 0) {
			return $arbitrajes->result();
		} else {
			return false;
		}
	}

	function eliminar($id_arb)
	{
		$this->db->where('id_arb', $id_arb);
		$return = $this->db->delete('arbitraje');
	}


	function actualizar($id_arb, $datos)
	{
		$this->db->where("id_arb", $id_arb);
		return $this->db->update("arbitraje", $datos);
	}
	function obtenerPorId($id_arb)
	{
		$this->db->where("id_arb", $id_arb);
		$arbitraje = $this->db->get("arbitraje");
		if ($arbitraje->num_rows() > 0) {
			return $arbitraje->row();
		} else {
			return false;
		}
	}
}
