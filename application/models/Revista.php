<?php
class Revista extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('revista', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$revistas = $this->db->get('revista');
		if ($revistas->num_rows() > 0) {
			return $revistas->result();
		} else {
			return false;
		}
	}

	function eliminar($id_rev)
	{
		$this->db->where('id_rev', $id_rev);
		$return = $this->db->delete('revista');
	}

	function consultarTodosConComite()
	{
		$this->db->select('revista.*, comite_editorial.nombre AS nombre');
		$this->db->from('revista');
		$this->db->join('comite_editorial', 'revista.EDITORIAL_ID = comite_editorial.id_comite');
		$query = $this->db->get();
		return $query->result();
	}
	function actualizar($id_rev, $datos)
	{
		$this->db->where("id_rev", $id_rev);
		return $this->db->update("revista", $datos);
	}
	function obtenerPorId($id_rev)
	{
		$this->db->select('revista.*, comite_editorial.nombre AS nombre');
		$this->db->from('revista');
		$this->db->join('comite_editorial', 'revista.EDITORIAL_ID = comite_editorial.id_comite');

		$this->db->where('revista.id_rev', $id_rev);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row();
		} else
			return false;
	}
}
