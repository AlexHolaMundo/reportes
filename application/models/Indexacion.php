<?php
class Indexacion extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('indexacion', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$indexaciones = $this->db->get('indexacion');
		if ($indexaciones->num_rows() > 0) {
			return $indexaciones->result();
		} else {
			return false;
		}
	}

	function eliminar($id_index)
	{
		$this->db->where('id_index', $id_index);
		$return = $this->db->delete('indexacion');
	}

	function consultarTodosConRevista()
	{
		$this->db->select('indexacion.*, revista.titulo AS titulo');
		$this->db->from('indexacion');
		$this->db->join('revista', 'indexacion.REVISTA_ID = revista.id_rev');
		$query = $this->db->get();
		return $query->result();
	}
	function actualizar($id_index, $datos)
	{
		$this->db->where("id_index", $id_index);
		return $this->db->update("indexacion", $datos);
	}
	function obtenerPorId($id_index)
	{
		$this->db->where("id_index", $id_index);
		$indexacion = $this->db->get("indexacion");
		if ($indexacion->num_rows() > 0) {
			return $indexacion->row();
		} else {
			return false;
		}
	}
}
