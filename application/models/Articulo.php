<?php
class Articulo extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('articulo', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$articulos = $this->db->get('articulo');
		if ($articulos->num_rows() > 0) {
			return $articulos->result();
		} else {
			return false;
		}
	}

	function eliminar($id_art)
	{
		$this->db->where('id_art', $id_art);
		$return = $this->db->delete('articulo');
	}

	function actualizar($id_art, $datos)
	{
		$this->db->where("id_art", $id_art);
		return $this->db->update("articulo", $datos);
	}

	function obtenerPorId($id_art)
	{
		$this->db->where("id_art", $id_art);
		$articulo = $this->db->get("articulo");
		if ($articulo->num_rows() > 0) {
			return $articulo->row();
		} else {
			return false;
		}
	}
}
