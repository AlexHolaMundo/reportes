<?php
class Documento extends CI_Model
{
	public $id_doc;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//metodo para insertar datos
	function insertar($datos)
	{
		$respuesta = $this->db->insert('documento', $datos);
		return $respuesta;
	}
	// Método para consultar los autores
	function consultarAutores()
	{
		return $this->db->get('autor')->result();
	}
	// Método para consultar los articulos
	public function consultarArticulos()
	{
		$query = $this->db->get('articulo');
		return $query->result();
	}
	// Método para consultar los revistas
	public function consultarRevistas()
	{
		$query = $this->db->get('revista');
		return $query->result();
	}
	// Método para consultar los revistas
	public function consultarArbitrajes()
	{
		$query = $this->db->get('arbitraje');
		return $query->result();
	}
	//metodo para consultar todos
	function consultarTodos()
	{
		$documentos = $this->db->get('documento');
		if ($documentos->num_rows() > 0) {
			return $documentos->result();
		} else {
			return false;
		}
	}
	public function consultarTodosConRelaciones()
	{
		$this->db->select('documento.id_doc, autor.nombre AS nombre_autor, autor.apellido AS apellido_autor, articulo.titulo AS titulo_articulo, revista.titulo AS titulo_revista, revista.issn AS issn_revista, revista.volumen AS volumen_revista, revista.numero AS numero_revista, revista.url AS url_revista, arbitraje.estado AS estado_arbitraje');
		$this->db->from('documento');
		$this->db->join('autor', 'autor.id_aut = documento.autor_id');
		$this->db->join('articulo', 'articulo.id_art = documento.articulo_id');
		$this->db->join('revista', 'revista.id_rev = documento.revista_id');
		$this->db->join('arbitraje', 'arbitraje.id_arb = documento.arbitraje_id');
		$query = $this->db->get();
		return $query->result();
	}
	public function obtenerDocumentoPorID($id_doc)
	{
		$this->db->select('*');
		$this->db->from('documento');
		$this->db->where('id_doc', $id_doc);
		$query = $this->db->get();
		return $query->row();
	}
}
