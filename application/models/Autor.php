<?php
class Autor extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('autor', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$autores = $this->db->get('autor');
		if ($autores->num_rows() > 0) {
			return $autores->result();
		} else {
			return false;
		}
	}

	function eliminar($id_aut)
	{
		$this->db->where('id_aut', $id_aut);
		$return = $this->db->delete('autor');
	}


	function actualizar($id_aut, $datos)
	{
		$this->db->where("id_aut", $id_aut);
		return $this->db->update("autor", $datos);
	}

	function obtenerPorId($id_aut)
	{
		$this->db->where("id_aut", $id_aut);
		$autor = $this->db->get("autor");
		if ($autor->num_rows() > 0) {
			return $autor->row();
		} else {
			return false;
		}
	}
}
