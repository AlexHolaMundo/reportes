<?php
class Comite_Editorial extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function insertar($datos)
	{
		$respuesta = $this->db->insert('comite_editorial', $datos);
		return $respuesta;
	}

	function consultarTodos()
	{
		$Comites_Editoriales = $this->db->get('comite_editorial');
		if ($Comites_Editoriales->num_rows() > 0) {
			return $Comites_Editoriales->result();
		} else {
			return false;
		}
	}

	function eliminar($id_comite)
	{
		$this->db->where('id_comite', $id_comite);
		$return = $this->db->delete('comite_editorial');
	}
	function obtenerPorId($id_comite)
	{
		$this->db->where("id_comite", $id_comite);
		$comite_editorial = $this->db->get("comite_editorial");
		if ($comite_editorial->num_rows() > 0) {
			return $comite_editorial->row();
		} else {
			return false;
		}
	}
	function actualizar($id_comite, $datos)
	{
		$this->db->where("id_comite", $id_comite);
		return $this->db->update("comite_editorial", $datos);
	}
}
