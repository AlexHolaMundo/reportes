<?php
class  Indexaciones extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Indexacion');
		$this->load->model('Revista');
	}

	public function index()

	{
		$data['listadoIndexaciones'] = $this->Indexacion->consultarTodosConRevista();
		$data['Revistas'] = $this->Revista->consultarTodos();
		$this->load->view("../views/templates/header");
		$this->load->view("indexaciones/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_index)
	{
		$this->Indexacion->eliminar($id_index);
		$this->session->set_flashdata('mensaje', 'Indexacion eliminada correctamente');
		redirect('indexaciones/index');
	}

	public function editar($id_index)
	{
		$data["indexacionEditar"] = $this->Indexacion->obtenerPorId($id_index);
		$data["Revistas"] = $this->Revista->consultarTodos();
		$this->load->view('../views/templates/header');
		$this->load->view('indexaciones/editar', $data);
		$this->load->view('../views/templates/footer');
	}

	public function guardar()
	{
		$config['upload_path'] = APPPATH . '../uploads/indexaciones/'; //ruta de subida de archivos
		$config['allowed_types'] = 'jpeg|jpg|png'; //tipo de archivos permitidos
		$config['max_size'] = 5 * 1024; //definir el peso maximo de subida (5MB)
		$nombre_aleatorio = "indexacion_" . time() * rand(100, 10000); //creando un nombre aleatorio
		$config['file_name'] = $nombre_aleatorio; //asignando el nombre al archivo subido
		$this->load->library('upload', $config); //cargando la libreria UPLOAD
		if ($this->upload->do_upload("logo")) { //intentando subir el archivo
			$dataArchivoSubido = $this->upload->data(); //capturando informacion del archivo subido
			$nombre_archivo_subido = $dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
		} else {
			$nombre_archivo_subido = ""; //Cuando no se sube el archivo el nombre queda VACIO
		}
		$datosNuevoIndexacion = array(
			"nombre" => $this->input->post("nombre"),
			"logo" => $nombre_archivo_subido,
			"revista_id" => $this->input->post("revista_id"),


		);
		$this->Indexacion->insertar($datosNuevoIndexacion);

		$this->session->set_flashdata('mensaje', 'Indexacion agregada Correctamente');
		redirect('indexaciones/index');
	}
	public function actualizarIndexacion()
	{
		$id = $this->input->post("id_index");
		$indexacion_existente = $this->Indexacion->obtenerPorId($id); // Cambiar $id_index por $id

		$config['upload_path'] = APPPATH . '../uploads/indexaciones/'; // Ruta de subida de archivos
		$config['allowed_types'] = 'jpeg|jpg|png'; // Tipo de archivos permitidos
		$config['max_size'] = 5 * 1024; // Peso máximo de subida (5MB)
		$nombre_aleatorio = "indexacion_" . time() * rand(100, 10000); // Creando un nombre aleatorio
		$config['file_name'] = $nombre_aleatorio; // Asignando el nombre al archivo subido
		$this->load->library('upload', $config); // Cargando la librería UPLOAD

		// Verificamos si se subió una nueva imagen
		if ($this->upload->do_upload("logo")) {
			$dataArchivoSubido = $this->upload->data(); // Capturando información del archivo subido
			$nombre_archivo_subido = $dataArchivoSubido["file_name"]; // Obteniendo el nombre del archivo
		} else {
			$nombre_archivo_subido = $indexacion_existente->logo; // Conservamos la foto existente
		}

		$datosIndexacion = array(
			"nombre" => $this->input->post("nombre"),
			"logo" => $nombre_archivo_subido,
			"revista_id" => $this->input->post("revista_id")
		);
		$this->Indexacion->actualizar($id, $datosIndexacion);
		$this->session->set_flashdata("mensaje", "Indexacion actualizado exitosamente");
		redirect('indexaciones/index');
	}
}
