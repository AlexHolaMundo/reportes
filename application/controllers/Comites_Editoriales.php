<?php
class Comites_Editoriales extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Comite_Editorial');
	}

	public function index()
	{
		$data['listadoComites'] = $this->Comite_Editorial->consultarTodos();
		$this->load->view("../views/templates/header");
		$this->load->view("Comites_Editoriales/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_comite)
	{
		$this->Comite_Editorial->eliminar($id_comite);
		$this->session->set_flashdata('mensaje', 'Comite Editorial eliminado correctamente');
		redirect('Comites_Editoriales/index');
	}
	public function editar($id_comite)
	{
		$data["comiteEditar"] = $this->Comite_Editorial->obtenerPorId($id_comite);
		$this->load->view('../views/templates/header');
		$this->load->view('Comites_Editoriales/editar', $data);
		$this->load->view('../views/templates/footer');
	}
	public function guardar()
	{
		$config['upload_path'] = APPPATH . '../uploads/comites/'; //ruta de subida de archivos
		$config['allowed_types'] = 'jpeg|jpg|png|pdf';
		$config['max_size'] = 5 * 1024; //definir el peso maximo de subida (5MB)
		$nombre_aleatorio = "comite_editorial_" . time() * rand(100, 10000); //creando un nombre aleatorio
		$config['file_name'] = $nombre_aleatorio; //asignando el nombre al archivo subido
		$this->load->library('upload', $config); //cargando la libreria UPLOAD
		if ($this->upload->do_upload("firma")) { //intentando subir el archivo
			$dataArchivoSubido = $this->upload->data(); //capturando informacion del archivo subido
			$nombre_archivo_subido = $dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
		} else {
			$nombre_archivo_subido = ""; //Cuando no se sube el archivo el nombre queda VACIO
		}
		$datosNuevoComite = array(
			"nombre" => $this->input->post("nombre"),
			"cargo_comite" => $this->input->post("cargo_comite"),
			"firma" => $nombre_archivo_subido,

		);
		$this->Comite_Editorial->insertar($datosNuevoComite);

		$this->session->set_flashdata('mensaje', 'Comite Editorial agregado Correctamente');
		redirect('Comites_Editoriales/index');
	}

	public function actualizar()
	{
		$id = $this->input->post("id_comite");
		$comite = $this->Comite_Editorial->obtenerPorId($id); // Cambiar $id_index por $id

		$config['upload_path'] = APPPATH . '../uploads/comites/'; // Ruta de subida de archivos
		$config['allowed_types'] = 'jpeg|jpg|png|pdf';
		$config['max_size'] = 5 * 1024; // Peso máximo de subida (5MB)
		$nombre_aleatorio = "comite_editorial_" . time() * rand(100, 10000); // Creando un nombre aleatorio
		$config['file_name'] = $nombre_aleatorio; // Asignando el nombre al archivo subido
		$this->load->library('upload', $config); // Cargando la librería UPLOAD

		// Verificamos si se subió una nueva imagen
		if ($this->upload->do_upload("firma")) {
			$dataArchivoSubido = $this->upload->data(); // Capturando información del archivo subido
			$nombre_archivo_subido = $dataArchivoSubido["file_name"]; // Obteniendo el nombre del archivo
		} else {
			$nombre_archivo_subido = $comite->firma; // Conservamos la foto existente
		}

		$datosComite = array(
			"nombre" => $this->input->post("nombre"),
			"cargo_comite" => $this->input->post("cargo_comite"),
			"firma" => $nombre_archivo_subido,
		);
		$this->Comite_Editorial->actualizar($id, $datosComite);
		$this->session->set_flashdata("mensaje", "Comite-Editorial actualizado exitosamente");
		redirect('Comites_Editoriales/index');
	}
}
