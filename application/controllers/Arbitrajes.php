<?php
class  Arbitrajes extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Arbitraje');
	}

	public function index()

	{
		$data['listadoArbitraje'] = $this->Arbitraje->consultarTodos();
		$this->load->view("../views/templates/header");
		$this->load->view("arbitrajes/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_arb)
	{
		$this->Arbitraje->eliminar($id_arb);
		$this->session->set_flashdata('mensaje', 'Arbitraje eliminada correctamente');
		redirect('arbitrajes/index');
	}

	public function editar($id_arb)
	{
		$data["arbitrajeEditar"] = $this->Arbitraje->obtenerPorId($id_arb);
		$this->load->view('../views/templates/header');
		$this->load->view('arbitrajes/editar', $data);
		$this->load->view('../views/templates/footer');
	}

	public function guardar()
	{
		$datosNuevoArbitraje = array(
			"estado" => $this->input->post("estado"),
			"comentario" => $this->input->post("comentario"),
			"fecha_desicion" => $this->input->post("fecha_desicion"),
		);
		$this->Arbitraje->insertar($datosNuevoArbitraje);

		$this->session->set_flashdata('mensaje', 'Arbitraje agregada Correctamente');
		redirect('arbitrajes/index');
	}
	public function actualizarArbitraje()
	{
		$id = $this->input->post("id_arb");
		$arbitraje_existente = $this->Arbitraje->obtenerPorId($id); // Cambiar $id_index por $id

		$datosArbitraje = array(
			"estado" => $this->input->post("estado"),
			"comentario" => $this->input->post("comentario"),
			"fecha_desicion" => $this->input->post("fecha_desicion"),
		);
		$this->Arbitraje->actualizar($id, $datosArbitraje);
		$this->session->set_flashdata("mensaje", "Arbitraje actualizado exitosamente");
		redirect('arbitrajes/index');
	}
}
