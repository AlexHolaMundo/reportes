<?php
class Revistas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Revista');
		$this->load->model('Comite_Editorial');
	}

	public function index()
	{
		$data['listadoRevistas'] = $this->Revista->consultarTodosConComite();
		$data["Comites_Editoriales"] = $this->Comite_Editorial->consultarTodos();
		$this->load->view("../views/templates/header");
		$this->load->view("revistas/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_rev)
	{
		$this->Revista->eliminar($id_rev);
		$this->session->set_flashdata('mensaje', 'Revista eliminada correctamente');
		redirect('revistas/index');
	}

	public function editar($id_rev)
	{
		$data["revistaEditar"] = $this->Revista->obtenerPorId($id_rev);
		$data["Comites_Editoriales"] = $this->Comite_Editorial->consultarTodos();
		$this->load->view('../views/templates/header');
		$this->load->view('revistas/editar', $data);
		$this->load->view('../views/templates/footer');
	}

	public function guardar()
	{
		$datosNuevoRevista = array(
			"titulo" => $this->input->post("titulo"),
			"issn" => $this->input->post("issn"),
			"volumen" => $this->input->post("volumen"),
			"numero" => $this->input->post("numero"),
			"url" => $this->input->post("url"),
			"editorial_id" => $this->input->post("editorial_id"),

		);
		$this->Revista->insertar($datosNuevoRevista);

		$this->session->set_flashdata('mensaje', 'Revista agregada Correctamente');
		redirect('revistas/index');
	}

	public function actualizar()
	{
		$id = $this->input->post("id_rev");

		$datosRevista = array(
			"titulo" => $this->input->post("titulo"),
			"issn" => $this->input->post("issn"),
			"volumen" => $this->input->post("volumen"),
			"numero" => $this->input->post("numero"),
			"url" => $this->input->post("url"),
			"editorial_id" => $this->input->post("editorial_id"),
		);

		$this->Revista->actualizar($id, $datosRevista);
		$this->session->set_flashdata(
			"mensaje",
			"Revista actualizada exitosamente"
		);
		redirect('revistas/index');
	}
}
