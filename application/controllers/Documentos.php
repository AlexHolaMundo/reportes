<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'libraries/tcpdf/tcpdf.php';

class Documentos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Documento');
		$this->load->library('form_validation');
		$this->load->model('Autor');
		$this->load->model('Articulo');
		$this->load->model('Revista');
		$this->load->model('Arbitraje');
	}

	public function index()
	{
		$data['listadoDocumentos'] = $this->Documento->consultarTodosConRelaciones();
		$data['listadoAutores'] = $this->Autor->consultarTodos();
		$data['listadoArticulos'] = $this->Articulo->consultarTodos();
		$data['listadoRevistas'] = $this->Revista->consultarTodos();
		$data['listadoArbitrajes'] = $this->Arbitraje->consultarTodos();
		$this->load->view("templates/header");
		$this->load->view("documentos/index", $data);
		$this->load->view("templates/footer");
	}

	public function guardar()
{
    $this->form_validation->set_rules('articulo_id', 'Artículo', 'required');
    $this->form_validation->set_rules('revista_id', 'Revista', 'required');
    $this->form_validation->set_rules('arbitraje_id', 'Arbitraje', 'required');

    if ($this->form_validation->run() == FALSE) {
        $this->session->set_flashdata('error', 'Todos los campos son obligatorios');
        redirect('documentos/index');
    } else {
        // Obtener los autores seleccionados
        $autores_seleccionados = $this->input->post('autor_id');
        
        // Verificar si se seleccionó al menos un autor
        if (!empty($autores_seleccionados)) {
            // Iterar sobre los autores seleccionados y guardarlos uno por uno
            foreach ($autores_seleccionados as $autor_id) {
                $datosNuevoDocumento = array(
                    'autor_id' => $autor_id,
                    'articulo_id' => $this->input->post('articulo_id'),
                    'revista_id' => $this->input->post('revista_id'),
                    'arbitraje_id' => $this->input->post('arbitraje_id')
                );
                $insertado = $this->Documento->insertar($datosNuevoDocumento);
                if (!$insertado) {
                    // Si falla la inserción para algún autor, mostrar error y redirigir
                    $this->session->set_flashdata('error', 'Error al agregar el informe');
                    redirect('documentos/index');
                }
            }
            // Si todas las inserciones fueron exitosas, mostrar mensaje y redirigir
            $this->session->set_flashdata('mensaje', 'Informes agregados correctamente');
            redirect('documentos/index');
        } else {
            // Si no se seleccionó ningún autor, mostrar error y redirigir
            $this->session->set_flashdata('error', 'Debes seleccionar al menos un autor');
            redirect('documentos/index');
        }
    }
}

	public function descargarPDF()
	{
		$listadoDocumentos = $this->Documento->consultarTodosConRelaciones();
		if ($listadoDocumentos) {
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('AlexHolaMundo');
			$pdf->SetTitle('Carta de aceptación de artículo científico');
			$pdf->SetSubject('Informe detallado de los artículos científicos aceptados');
			$pdf->SetKeywords('PDF, carta, aceptación, artículo, científico');
			$pdf->SetHeaderData('../assets/img/user.png', 20, 'Created By:', 'AlexHolaMundo', array(0, 64, 0), array(0, 64, 128));
			$pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));
			$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
				require_once(dirname(__FILE__) . '/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->setFontSubsetting(true);
			$pdf->SetFont('dejavusans', '', 14, '', true);
			foreach ($listadoDocumentos as $documento) {
				$texto_documento = strtoupper($documento->titulo_revista);
				setlocale(LC_TIME, 'spanish');
				$fecha_actual = strftime("%e de %B del %Y");
				$html = <<<EOD
				<style>
				.revistatitle {
					font-size: 18px;
					font-weight: bold;
					text-align: right;
					color: blue;
					text-transform: uppercase;
				}
				h6 {
					text-align: right;
				}
				.autores{
					font-size: 15px;
					font-weight: bold;
				}
				.presentes {
					font-size: 14px;
					font-weight: bold;
				}
				p {
					font-size: 15px;
				}
				a {
					color: blue;
					text-decoration: none;
				}
				.articulo{
					font-size: 17px;
					font-weight: bold;
					text-align: center;
				}
				.url{
					color: blue;
					text-decoration: none;
					font-size: 14px;
				}
				.fin{
					text-align: center;
					font-size: 14px;
				}
				</style>
				<p class="revistatitle">REVISTA {$documento->titulo_revista}</p>
				<h6>Latacunga, {$fecha_actual}</h6>

				<p class="autores">{$documento->nombre_autor} {$documento->apellido_autor}</p>

				<p class="presentes">Presentes</p>
				<p>Reciban un cordial saludo y sirve este medio para informarle que, una vez realizo el proceso de arbitraje, el Comité Editorial de la revista {$texto_documento} ha decidido publicar su artículo titulado:
				</p>
				<p class="articulo">
					"{$documento->titulo_articulo}"
				</p>

				<p>Mismo que cumple con los lineamientos estipulados para la publicación.</p>
				<p>Su artículo es presentado en forma digital y formato PDF que se incluye en el volumen {$documento->volumen_revista}, número {$documento->numero_revista} de nuestra revista con ISSN: {$documento->issn_revista}. y dirección electrónica:
				</p>
				<a class="url" href="">{$documento->url_revista}</a>
				<p>El comité editorial de la revista {$texto_documento} agradece su participación y le invita 	a seguir con nosotros ya que es grato contar con valiosas aportaciones.
				</p>

				<h5 class="fin">Sin otro particular, le saluda.</h5>
				<h5 class="fin">ATENTAMENTE</h5>

				<p class="fin">Comité Editorial</p>
				<p class="fin">Revista VICTEC</p>
				EOD;
				$pdf->AddPage();
				$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
				$pdf->writeHTML($html, true, false, true, false, '');
			}
			$pdf->Output('Carta_Aceptacion.pdf', 'I');
		} else {
			echo "No se encontraron documentos para generar PDF.";
		}
	}
}
