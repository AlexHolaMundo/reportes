<?php
class Autores extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Autor');
	}
	//Renderizar la vista de la lista de autores
	public function index()
	{
		$data['listadoAutores'] = $this->Autor->consultarTodos();
		$this->load->view("../views/templates/header");
		$this->load->view("autores/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_aut)
	{
		$this->Autor->eliminar($id_aut);
		$this->session->set_flashdata('mensaje', 'Autor eliminado correctamente');
		redirect('autores/index');
	}

	public function editar($id_aut)
	{
		$data["autorEditar"] = $this->Autor->obtenerPorId($id_aut);
		$this->load->view('../views/templates/header');
		$this->load->view('autores/editar', $data);
		$this->load->view('../views/templates/footer');
	}

	public function guardar()
	{
		$datosNuevoAutor = array(
			"nombre" => $this->input->post("nombre"),
			"apellido" => $this->input->post("apellido"),
			"email" => $this->input->post("email"),
			"pais" => $this->input->post("pais"),
			"ciudad" => $this->input->post("ciudad"),
			"telefono" => $this->input->post("telefono"),

		);
		$this->Autor->insertar($datosNuevoAutor);

		$this->session->set_flashdata('mensaje', 'Autor agregado Correctamente');
		redirect('autores/index');
	}
	public function actualizarAutor()
	{
		$id_aut = $this->input->post("id_aut");
		$datosAutor = array(
			"nombre" => $this->input->post("nombre"),
			"apellido" => $this->input->post("apellido"),
			"email" => $this->input->post("email"),
			"pais" => $this->input->post("pais"),
			"ciudad" => $this->input->post("ciudad"),
			"telefono" => $this->input->post("telefono"),
		);
		$this->Autor->actualizar($id_aut, $datosAutor);
		$this->session->set_flashdata("mensaje", "Autor actualizado exitosamente");
		redirect('autores/index');
	}
}
