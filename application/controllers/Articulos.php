<?php
class Articulos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Articulo');
	}

	public function index()
	{
		$data['listadoArticulos'] = $this->Articulo->consultarTodos();

		$this->load->view("../views/templates/header");
		$this->load->view("articulos/index", $data);
		$this->load->view("../views/templates/footer");
	}

	public function borrar($id_art)
	{
		$this->Articulo->eliminar($id_art);
		$this->session->set_flashdata('mensaje', 'Articulo eliminado correctamente');
		redirect('articulos/index');
	}

	public function editar($id_art)
	{
		$data["articuloEditar"] = $this->Articulo->obtenerPorId($id_art);
		$this->load->view('../views/templates/header');
		$this->load->view('articulos/editar', $data);
		$this->load->view('../views/templates/footer');
	}


	public function actualizarArticulo()
	{
		$id_art = $this->input->post("id_art");
		$datosArticulo = array(
			"titulo" => $this->input->post("titulo"),
			"fecha" => $this->input->post("fecha"),

		);
		$this->Articulo->actualizar($id_art, $datosArticulo);
		$this->session->set_flashdata("mensaje", "Articulo actualizado exitosamente");
		redirect('articulos/index');
	}

	public function guardar()
	{
		$datosNuevoArticulo = array(
			"titulo" => $this->input->post("titulo"),
			"fecha" => $this->input->post("fecha"),
		);
		$this->Articulo->insertar($datosNuevoArticulo);

		$this->session->set_flashdata('mensaje', 'Articulo agregado Correctamente');
		redirect('articulos/index');
	}
}
